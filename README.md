# LibProperties

A small netsandard2.0 library that adds generic bindable properties (somewhat inspired by properties from JavaFX).

## Example

Source:

```csharp
using LibProperties;

public class Subject {
    public Property<int> IntProperty {get;}
    public Property<bool> BoolProperty {get;}
    public Property<float> FloatProperty {get;}
    public Property<string> StringProperty {get;}
    public Property<Subject> SubjectProperty {get;}
    public int Id {get;}
    
    private static int counter;

    public Subject() {
        IntProperty = new Property<int>();
        BoolProperty = new Property<bool>();
        FloatProperty = new Property<float>();
        StringProperty = new Property<string>();
        SubjectProperty = new Property<Subject>();
        Id = counter++;
    }
}
```

```csharp
static void Main(string[] args) {
    var A = new Subject();
    var B = new Subject();
    var C = new Subject();

    Console.WriteLine("Subjects created");
    Console.WriteLine("  A.Id: {0}", A.Id);
    Console.WriteLine("  B.Id: {0}", B.Id);
    Console.WriteLine("  C.Id: {0}", C.Id);

    // Unidirectional bind C.IntProperty -> A.IntProperty
    A.IntProperty.Subscribe(C.IntProperty);

    Console.WriteLine("C.IntProperty subscribed to A.IntProperty");

    // Bidirectional bind A.IntProperty <-> B.IntProperty
    A.IntProperty.Subscribe(B.IntProperty);
    B.IntProperty.Subscribe(A.IntProperty);

    Console.WriteLine("B.IntProperty subscribed to A.IntProperty");
    Console.WriteLine("A.IntProperty subscribed to B.IntProperty");

    // Unidirectional bind of a reference type B.SubjectProperty -> A.SubjectProperty
    A.SubjectProperty.Subscribe(B.SubjectProperty);

    Console.WriteLine("B.SubjectProperty subscribed to A.SubjectProperty");

    // Unidirectional bind of a reference type C.SubjectProperty -> B.SubjectProperty
    B.SubjectProperty.Subscribe(C.SubjectProperty);

    Console.WriteLine("C.SubjectProperty subscribed to B.SubjectProperty");

    // Updating A.IntProperty -> 42
    A.IntProperty.Value = 42;

    Console.WriteLine("Update: A.IntProperty -> 42");
    Console.WriteLine("  A.IntProperty: {0}", A.IntProperty);
    Console.WriteLine("  B.IntProperty: {0}", B.IntProperty);
    Console.WriteLine("  C.IntProperty: {0}", C.IntProperty);

    // Updating A.SubjectProperty -> C
    A.SubjectProperty.Value = C;

    Console.WriteLine("Update: A.SubjectProperty -> C");
    Console.WriteLine("  A.SubjectProperty: {0}", A.SubjectProperty.Value.Id);
    Console.WriteLine("  B.SubjectProperty: {0}", B.SubjectProperty.Value.Id);
    Console.WriteLine("  C.SubjectProperty: {0}", C.SubjectProperty.Value.Id);

    // Unbind C.IntProperty -> A.IntProperty
    A.IntProperty.Unsubscribe(C.IntProperty);

    Console.WriteLine("C.IntProperty unsubscribed from A.IntProperty");

    // Updating B.IntProperty -> 32
    B.IntProperty.Value = 32;

    Console.WriteLine("Update: B.IntProperty -> 32");
    Console.WriteLine("  A.IntProperty: {0}", A.IntProperty);
    Console.WriteLine("  B.IntProperty: {0}", B.IntProperty);
    Console.WriteLine("  C.IntProperty: {0}", C.IntProperty);

    // Unidirectional bind C.FloatProperty -> A.FloatProperty
    A.FloatProperty.Subscribe(C.FloatProperty);

    Console.WriteLine("C.FloatProperty subscribed to A.FloatProperty");

    // Unidirectional bind C.FloatProperty -> B.FloatProperty
    B.FloatProperty.Subscribe(C.FloatProperty);

    Console.WriteLine("C.FloatProperty subscribed to B.FloatProperty");

    // Updating A.FloatProperty -> 1
    A.FloatProperty.Value = 1;

    // Updating B.FloatProperty -> 2
    B.FloatProperty.Value = 2;

    Console.WriteLine("Update: A.FloatProperty -> 1");
    Console.WriteLine("Update: B.FloatProperty -> 2");
    Console.WriteLine("  A.FloatProperty: {0}", A.FloatProperty);
    Console.WriteLine("  B.FloatProperty: {0}", B.FloatProperty);
    Console.WriteLine("  C.FloatProperty: {0}", C.FloatProperty);

    // Unsubscribe all properties subscribed to A.SubjectProperty
    A.SubjectProperty.Unsubscribe();

    Console.WriteLine("All properties unsubscribed from A.SubjectProperty");

    // Updating A.SubjectProperty -> B
    A.SubjectProperty.Value = B;

    Console.WriteLine("Update: A.SubjectProperty -> B");
    Console.WriteLine("  A.SubjectProperty: {0}", A.SubjectProperty.Value.Id);
    Console.WriteLine("  B.SubjectProperty: {0}", B.SubjectProperty.Value.Id);
    Console.WriteLine("  C.SubjectProperty: {0}", C.SubjectProperty.Value.Id);
}
```

Output:

```txt
Subjects created
  A.Id: 0
  B.Id: 1
  C.Id: 2
C.IntProperty subscribed to A.IntProperty
B.IntProperty subscribed to A.IntProperty
A.IntProperty subscribed to B.IntProperty
B.SubjectProperty subscribed to A.SubjectProperty
C.SubjectProperty subscribed to B.SubjectProperty
Update: A.IntProperty -> 42
  A.IntProperty: 42
  B.IntProperty: 42
  C.IntProperty: 42
Update: A.SubjectProperty -> C
  A.SubjectProperty: 2
  B.SubjectProperty: 2
  C.SubjectProperty: 2
C.IntProperty unsubscribed from A.IntProperty
Update: B.IntProperty -> 32
  A.IntProperty: 32
  B.IntProperty: 32
  C.IntProperty: 42
C.FloatProperty subscribed to A.FloatProperty
C.FloatProperty subscribed to B.FloatProperty
Update: A.FloatProperty -> 1
Update: B.FloatProperty -> 2
  A.FloatProperty: 1
  B.FloatProperty: 2
  C.FloatProperty: 2
All properties unsubscribed from A.SubjectProperty
Update: A.SubjectProperty -> B
  A.SubjectProperty: 1
  B.SubjectProperty: 2
  C.SubjectProperty: 2
```
