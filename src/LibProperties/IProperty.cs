using System.ComponentModel;
using System;

namespace LibProperties {
    public interface IProperty<T> : INotifyPropertyChanged, IObservable<T>, IObserver<T>, IDisposable {
        bool Unsubscribe();
        bool Unsubscribe(IObserver<T> observer);
    }
}