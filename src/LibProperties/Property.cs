using System.Collections.Generic;
using System.ComponentModel;
using System;

namespace LibProperties {
    public sealed class Property<T> : IProperty<T> {
        internal HashSet<IObserver<T>> _Observer = new HashSet<IObserver<T>>();

        public T Value {
            get => value;
            set => SetValue(value);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private T value;

        public Property() : this(default(T)) {}

        public Property(T initialValue) {
            value = initialValue;

            PropertyChanged += (s, a) => {
                var value = Value;

                foreach(var o in _Observer)
                    o.OnNext(value);
            };
        }

        /// <summary>
        /// Subscribes the given Observer to this Observable.
        /// </summary>
        /// <param name="observer">The Observer to subscribe</param>
        /// <returns>This Observable</returns>
        public IDisposable Subscribe(IObserver<T> observer) {
            _Observer.Add(observer);
            return this;
        }

        /// <summary>
        /// Unsubscribes all Observers that are subscribed 
        /// to this Observable.
        /// </summary>
        /// <returns>Wether atleast one Observer was unsubscribed</returns>
        public bool Unsubscribe() {
            if(_Observer.Count > 0) {
                _Observer.Clear();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Unsubscribes the given Observer from this Observable.
        /// </summary>
        /// <param name="observable">Observer to unsubscribe</param>
        /// <returns>Wether the Observer was unsubscribed</returns>
        public bool Unsubscribe(IObserver<T> observer) {
            return _Observer.Remove(observer);
        }

        public void OnCompleted() {
            // no op
        }

        public void OnError(Exception error) {
            throw new NotImplementedException();
        }

        public void OnNext(T value) {
            Value = value;
        }

        public void Dispose() {
            _Observer.Clear();
        }

        public override string ToString() {
            var value = Value;

            return value != null
                ? value.ToString() : "null";
        }

        private void SetValue(T value) {
            // only do something if the new value is actually different
            if(!(value != null ? value.Equals(this.value)
            : this.value != null && this.value.Equals(value))) {
                this.value = value;
                OnPropertyChanged();
            }
        }

        private void OnPropertyChanged() =>
            PropertyChanged?.Invoke(this, null);

        public static implicit operator T(Property<T> property) {
            return property.Value;
        }
    }
}