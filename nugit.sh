#!/bin/bash.sh

function print_usage {
    echo "usage: $0 (-u|--update) ((-j|--major)|(-r|--minor)|(-p|--patch)) (-m|--message) \"<tag-message>\"[ --no-push]"
    echo "       $0 (-s|--set) <new-tag> (-m|--message) \"<tag-mesage>\"[ --no-push]"
    echo "       $0[ --no-push]"
}

# https://stackoverflow.com/a/17364637
# Accepts a version string and prints it incremented by one.
# Usage: increment_version <version> [<position>] [<leftmost>]
increment_version() {
   local usage=" USAGE: $FUNCNAME [-l] [-t] <version> [<position>] [<leftmost>]
           -l : remove leading zeros
           -t : drop trailing zeros
    <version> : The version string.
   <position> : Optional. The position (starting with one) of the number 
                within <version> to increment.  If the position does not 
                exist, it will be created.  Defaults to last position.
   <leftmost> : The leftmost position that can be incremented.  If does not
                exist, position will be created.  This right-padding will
                occur even to right of <position>, unless passed the -t flag."

   # Get flags.
   local flag_remove_leading_zeros=0
   local flag_drop_trailing_zeros=0
   while [ "${1:0:1}" == "-" ]; do
      if [ "$1" == "--" ]; then shift; break
      elif [ "$1" == "-l" ]; then flag_remove_leading_zeros=1
      elif [ "$1" == "-t" ]; then flag_drop_trailing_zeros=1
      else echo -e "Invalid flag: ${1}\n$usage"; return 1; fi
      shift; done

   # Get arguments.
   if [ ${#@} -lt 1 ]; then echo "$usage"; return 1; fi
   local v="${1}"             # version string
   local targetPos=${2-last}  # target position
   local minPos=${3-${2-0}}   # minimum position

   # Split version string into array using its periods. 
   local IFSbak; IFSbak=IFS; IFS='.' # IFS restored at end of func to                     
   read -ra v <<< "$v"               #  avoid breaking other scripts.

   # Determine target position.
   if [ "${targetPos}" == "last" ]; then 
      if [ "${minPos}" == "last" ]; then minPos=0; fi
      targetPos=$((${#v[@]}>${minPos}?${#v[@]}:$minPos)); fi
   if [[ ! ${targetPos} -gt 0 ]]; then
      echo -e "Invalid position: '$targetPos'\n$usage"; return 1; fi
   (( targetPos--  )) || true # offset to match array index

   # Make sure minPosition exists.
   while [ ${#v[@]} -lt ${minPos} ]; do v+=("0"); done;

   # Increment target position.
   v[$targetPos]=`printf %0${#v[$targetPos]}d $((10#${v[$targetPos]}+1))`;

   # Remove leading zeros, if -l flag passed.
   if [ $flag_remove_leading_zeros == 1 ]; then
      for (( pos=0; $pos<${#v[@]}; pos++ )); do
         v[$pos]=$((${v[$pos]}*1)); done; fi

   # If targetPosition was not at end of array, reset following positions to
   #   zero (or remove them if -t flag was passed).
   if [[ ${flag_drop_trailing_zeros} -eq "1" ]]; then
        for (( p=$((${#v[@]}-1)); $p>$targetPos; p-- )); do unset v[$p]; done
   else for (( p=$((${#v[@]}-1)); $p>$targetPos; p-- )); do v[$p]=0; done; fi

   echo "${v[*]}"
   IFS=IFSbak
   return 0
}

# 0: current, 1: update, 2: set
OPTION=0

# parse user input
while [[ $# -gt 0 ]]
do
    case "$1" in
        -u|--update)
            UPDATE_TARGET="$2"
            OPTION=1
            shift
            shift
        ;;

        -s|--set)
            TAG="$2"
            OPTION=2
            shift
            shift
        ;;

        -d|--description)
            DESCRIPTION="$2"
            shift
            shift
        ;;

        --no-push)
            NO_RELEASE=1
            shift
        ;;

        *)
            echo "invalid argument '$1'"
            print_usage
            exit 1
        ;;
    esac
done

if [ "$OPTION" -eq 0 ]
then
    # get current tag and version
    TAG=$(git describe)
    VERSION=${TAG:1}
elif [ "$OPTION" -eq 1 ]
then
    # get latest tag and version
    TAG=$(git describe --abbrev=0)
    VERSION=${TAG:1}

    if [[ $VERSION =~ -.+$ ]]
    then
        EXTENSION=$BASH_REMATCH
    fi

    case "$UPDATE_TARGET" in
        -m|--major)
            VERSION=$(increment_version -l "$VERSION" 1)$EXTENSION
        ;;

        -r|--minor)
            VERSION=$(increment_version -l "$VERSION" 2)$EXTENSION
        ;;

        -p|--patch)
            VERSION=$(increment_version -l "$VERSION" 3)$EXTENSION
        ;;

        *)
            echo "invalid update target '$UPDATE_TARGET'"
            exit 1
        ;;
    esac

    TAG=v"$VERSION"
elif [ "$OPTION" -eq 2 ]
then
    # get version of new tag
    VERSION=${TAG:1}
fi

if [[ ! "$TAG" =~ ^v[0-9]+\.[0-9]+\.[0-9]+(-.+)?$ ]]
then
    echo "invalid tag '$TAG'"
    exit 1
fi

# update version in csproj
sed -i -e "s/<Version>.*<\/Version>/<Version>$VERSION<\/Version>/g" *.csproj

# add and commit changes to csproj
git add *.csproj
git commit -m "Updated version"

if [ "$OPTION" -ne 0 ]
then
    if [ -z "$DESCRIPTION" ]
    then
        echo "description may not be empty"
        exit 1
    fi

    # create new tag
    git tag -a "$TAG" -m "$DESCRIPTION"

    # push changes to remote
    git push --atomic origin HEAD "$TAG"

    if [ "$?" -ne 0 ]
    then
        git reset --hard HEAD~1
        git tag -d $TAG
        echo "push failed, commit was reverted and created tag deleted"
        exit 1
    fi
elif [ "$?" -eq 0 ]
then
    git push

    if [ "$?" -ne 0 ]
    then
        git reset --hard HEAD~1
        echo "push failed, commit was reverted"
        exit 1
    fi
fi

# create nuget package
dotnet pack -c Release

# push package to nuget
# todo